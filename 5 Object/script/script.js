 /*
 Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата".
 Создать в объекте вложенный объект - "Приложение"Приложение. 
 Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата". 
 Создать методы для заполнения и отображения документа.
 */

 //Создаем объект "Документ" - doc.
 const doc = {
    header:"",
    body:"",
    footer:"",
    date:"",
    application: {
        header:"",
        body: "",
        footer: "",
        date: ""
    },

    //Создаем метод вывода - doc.
    show: function () {
        document.write("<h1> Приказ № " + doc.header + " .</h1>");
        document.write("<p> &nbsp &nbsp &nbsp &nbsp" + this.body + ".</p>");
        document.write("<div>" + this.footer + ".</div>");
        document.write("<blockquote>" + this.date + ".</blockquote>");
        document.write("<h2> Приложение № " + this.application.header + " к Приказу № " + this.header + " .</h2>");
        document.write("<p> &nbsp &nbsp &nbsp &nbsp" + this.application.body + ".</p>");
        document.write("<div>" + this.application.footer + ".</div>");
        document.write("<blockquote>" + this.application.date + ".</blockquote>");
    },

    //Создаем метод заполнения - doc.
    fill: function (){
        doc.header = prompt("Введите № приказа","")
        doc.body = prompt("Введите текст приказа","");
        doc.footer = prompt("Введите должность подписанта приказа","");
        doc.date = prompt("Введите дату составления приказа","");
        doc.application.header = prompt("Введите № приложения","")
        doc.application.body = prompt("Введите текст приложения","");
        doc.application.footer = prompt("Введите должность подписанта приложения","");
        doc.application.date = prompt("Введите дату составления приложения","");
    },
}

//Вносим данные в элементы объекта - doc.
doc.fill();

//Выводим все элементы объекта - doc.
doc.show();

