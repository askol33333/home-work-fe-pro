import React from "react";
import ReactDOM from "react-dom";

const ZadiakSigns = ()=>{
    return (
        <>
            <h1>Знаки зодиака</h1>
            <li>Овен (21 марта – 19 апреля)<br></br><img width="200px" src="https://upload.wikimedia.org/wikipedia/commons/e/e6/RR5110-0049R.gif"></img></li>
            <li>Телец (20 апреля — 20 мая)<br></br><img width="200px" src="https://upload.wikimedia.org/wikipedia/commons/7/71/RR5110-0050R.gif"></img></li>
            <li>Близнецы (21 мая – 20 июня)<br></br><img width="200px" src="https://upload.wikimedia.org/wikipedia/commons/1/10/RR5110-0051R.gif"></img></li>
            <li>Рак (21 июня — 22 июля)<br></br><img width="200px" src="https://upload.wikimedia.org/wikipedia/commons/c/cf/RR5110-0052R.gif"></img></li>
            <li>Лев (23 июля — 22 августа)<br></br><img width="200px" src="https://upload.wikimedia.org/wikipedia/commons/b/b7/RR5110-0040R.gif"></img></li>
            <li>Дева (23 августа — 22 сентября)<br></br><img width="200px" src="https://tattooinfo.ru/800/600/https/cdn.monetnik.ru/storage/market-lot/90/30/144190/458572_big.jpg"></img></li>
            <li>Весы (23 сентября — 22 октября)<br></br><img width="200px" src="https://upload.wikimedia.org/wikipedia/commons/b/b6/Coin_of_the_Bank_of_Russia_-_Libra_2_rubles_2002_reverse.gif"></img></li>
            <li>Скорпион (23 октября — 21 ноября)<br></br><img width="200px" src="https://upload.wikimedia.org/wikipedia/commons/8/8a/RR5110-0043R.gif"></img></li>
            <li>Стрелец (22 ноября — 21 декабря)<br></br><img width="200px" src="https://upload.wikimedia.org/wikipedia/commons/b/b2/RR5111-0119R.gif"></img></li>
            <li>Козерог (22 декабря — 19 января)<br></br><img width="200px" src="https://upload.wikimedia.org/wikipedia/commons/c/c1/RR5110-0045R.gif"></img></li>
            <li>Водолей (20 января — 18 февраля)<br></br><img width="200px" src="https://upload.wikimedia.org/wikipedia/commons/4/44/RR5111-0122R.gif"></img></li>
            <li>Рыбы (19 февраля – 20 марта)<br></br><img width="200px" src="https://upload.wikimedia.org/wikipedia/commons/c/c7/RR5110-0048R.gif"></img></li>
        </>
    )
}

ReactDOM.render(<ZadiakSigns/>, document.querySelector("#root"));

