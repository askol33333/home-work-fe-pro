
// Запускаем главную функцию кода (init) после загрузки контента страницы.
window.addEventListener("DOMContentLoaded", init);

    let pageIndex = 1, idHero;

// Объявляем главную функцию кода (init).
function init() {
    const loader = document.querySelector('.loader'),
          boxCard = document.createElement('div'),
          pagination = document.querySelector(`.pagination`);

    // Организация стартового запроса на сервер.    
    const hrx = new XMLHttpRequest ();
    hrx.open('GET',`https://swapi.dev/api/people/`);
    hrx.send();
    console.log(hrx);
    hrx.addEventListener("readystatechange", () =>{
        if (hrx.readyState == 4 && hrx.status == 200){
            console.log(JSON.parse(hrx.responseText));
            show(JSON.parse(hrx.responseText));
            loader.style.display = 'none';
        }
    })
    
    // Слушаем событие нажатия на ленту номеров страниц.
    pagination.addEventListener("click", (e)=>{
        pageIndex = e.target.innerText;
    
        // Организация запроса на сервер.    
        loader.style.display = '';
        const hrx = new XMLHttpRequest ();
        hrx.open('GET',`https://swapi.dev/api/people/?page=${pageIndex}`);
        hrx.send();
        console.log(hrx);
        hrx.addEventListener("readystatechange", () =>{
            if (hrx.readyState == 4 && hrx.status == 200){
                
                //Удаляем 10 карточек ранне выведеных на экран.
                let allCard = document.querySelectorAll(".card");
                allCard.forEach((el)=>{
                    el.remove();
                })
                
                show(JSON.parse(hrx.responseText));
                loader.style.display = 'none';
            }
        })    
    })
    
    
    //Функция создания карточек и вывода их на экран.
    function show(date){
        boxCard.style.display="flex"; 
        loader.after(boxCard);
        
        date.results.forEach((el, i)=>{
            const card = document.createElement('div'),
                cardBody = document.createElement('div'),
                cardTitle = document.createElement('h5'),
                cardSubtitle = document.createElement('h6'),
                cardText = document.createElement('p'),
                cardLink1 = document.createElement('a'),
                cardLink2 = document.createElement('a');
      
            card.classList.add("card");
            card.style.width = "18rem";
            cardBody.classList.add("card-body");
            cardTitle.classList.add("card-title");
            cardSubtitle.classList.add("card-subtitle","mb-2","text-muted");
            cardText.classList.add("card-text");
            cardLink1.classList.add("card-link");
            pageIndex === 1 ? idHero = (i+1) : idHero = ((pageIndex-1)*10)+(i+1);
            
            //Условная конструкция обходящая ошибку на сервере с ID17.
            if (idHero >= 17) {
                idHero = ((pageIndex-1)*10)+(i+2);
            } 
            
            cardLink1.setAttribute('href',`https://swapi.dev/api/people/${idHero}/`);
            cardLink2.classList.add("card-link");
            cardLink2.setAttribute('href',el.homeworld);
  
            cardTitle.innerText = el.name;
            cardSubtitle.innerText = el.gender;
            cardText.innerText = el.homeworld;
            cardLink1.innerText= 'О ГЕРОЕ';
            cardLink2.innerText= 'РОДНАЯ ПЛАНЕТА';

            boxCard.append(card);
            card.prepend(cardBody);
            cardBody.append(cardTitle);
            cardBody.append(cardSubtitle);
            cardBody.append(cardText);
            cardBody.append(cardLink1); 
            cardBody.append(cardLink2);
        })
    }
}

