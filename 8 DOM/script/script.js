/*  При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". 
Данная кнопка должна являться единственным контентом в теле HTML документа, 
весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript.
    При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. 
При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. 
При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, 
то есть все остальные круги сдвигаются влево.
*/

window.onload = function() {
    document.getElementById("btn").onclick = function() {
        //Создаем поле для ввода диаметра круга.
        let circleDiametr = document.createElement("input");
        circleDiametr.setAttribute("type", "text");
        circleDiametr.setAttribute("class", "input"); 
        circleDiametr.value = "100";
        btn.after(circleDiametr);

        //Создаем кнопку "Нарисовать".
        let circle100Paint = document.createElement("input");
        circle100Paint.setAttribute("type", "button"); 
        circle100Paint.setAttribute("class", "button"); 
        circle100Paint.setAttribute("value", "Нарисовать"); 
        circleDiametr.after(circle100Paint);
        
        //Логика появления 100 ед кругов на экране.
        circle100Paint.onclick = function() {
            document.body.innerText = "";
            let box = document.createElement("div");
            box.style.cssText = `
                margin: 20px;
                display: flex;
                flex-wrap: wrap;`;
            box.style.width = 10*circleDiametr.value + 100 + "px"; 
            document.body.append(box);

            //Перебираем цикл создания 100 ед. кругов.
            for(let i = 0; i < 100; i++) {
                let circle = document.createElement("div");
                circle.style.backgroundColor = getColor();
                circle.style.width = circleDiametr.value + "px";
                circle.style.height = circleDiametr.value + "px";
                circle.style.marginLeft = "10px";
                circle.style.marginTop = "10px";
                circle.style.borderRadius = "50%";
                box.append(circle); 
                 
                //Логика удаления круга.
                circle.onclick = (e) => {
                    e.target.remove()
                }
            }
        }        
    }
}

//Функция установки произвольного цвета круга.
function getColor() {
    return `hsl(${Math.floor(Math.random()*360)}, 50%, 50%)`;
}
