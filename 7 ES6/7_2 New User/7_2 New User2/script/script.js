/* 
1. Реализовать функцию для создания объекта "пользователь".
Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser. 
При вызове функция должна спросить у вызывающего имя и фамилию. Используя данные, введенные пользователем, 
создать объект newUser со свойствами firstName и lastName. Добавить в объект newUser метод getLogin(),
который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, 
все в нижнем регистре (например, Ivan Kravchenko → ikravchenko). Создать пользователя с помощью функции createNewUser().
Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.
    
2.Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее следующим функционалом: 
При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday. 
Создать метод getAge() который будет возвращать сколько пользователю лет. Создать метод getPassword(), который будет 
возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. 
(например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992). Вывести в консоль результат работы функции createNewUser(), 
а также функций getAge() и getPassword() созданного объекта.
*/

//2.
//Создаем функцию (class) createNewUser.
class createNewUser {
  constructor (firstName, lastName, birthDay){
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDay = birthDay;
  }
  
  // Создаем метод getLogin() с логикой - возвращать первую букву имени пользователя, соединенную с фамилией пользователя все в нижнем регистре.
  getLogin(){
    return this.firstName.substr(0, 1).toLowerCase() + this.lastName.toLowerCase();
  }

  // Создаем метод getAge() с логикой - подсчет возраста пользователя. 
  getAge() {
    return ((new Date().getTime() - new Date(this.birthDay.reverse()).getTime()) / (24 * 3600 * 365.25 * 1000)) | 0;
  }  

  //Создаем метод getPassword() с логикой - возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения.
  getPassword(){
    return this.firstName.substr(0, 1).toUpperCase() + this.lastName.toLowerCase() + this.birthDay[0];
  }
}

//Вызываем функцию (class) createNewUser и через модальные окошки пользователь вводит имя и фамилию, 
//и создаем объект(экземпляр) NewUser.
let NewUser = new createNewUser(prompt("Введите имя", "Ivan"), prompt("Введите фамилию", "Ivanov"), prompt("Введите дату рождения фамилию", "dd.mm.yyyy").split('.'));

//Выводим в консоль результат выполнения кода.
console.log(NewUser.getLogin());
console.log(NewUser.getAge());
console.log(NewUser.getPassword());
