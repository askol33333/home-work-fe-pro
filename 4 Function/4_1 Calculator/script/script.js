/*Создайте калькулятор

 За каждую операцию будет отвечать отдельная функция,
 т.е. для сложения - add(a,b), для умножения - multiple(a,b) и т.д.
 Каждая из них принимает в аргументы только два числа и возвращает результат операции над двумя числами
 Если число не передано в функцию аргументом - ПО УМОЛЧАНИЮ присваивать этому аргументу 0.
				  
Основная функция calculate()
Принимает ТРИ АРГУМЕНТА:
 1 - число
 2 - число
 3 - функция которую нужно выполнить для двух этих чисел.
Таким образом получается что основная функция калькулятор будет 
вызывать переданную ей аргументом функцию для двух чисел, которые передаются 
остальными двумя аргументами. При делении на 0 выводить ошибку.
Функия калькулятор доджна принять на вход 3 аругмента, 
Если аргументов больше или меньше выводить ошибку.
*/

var a, b, х1, х2, sign, result;

var btn = document.querySelector('input[type=button][value=start]');

//Функция - сложение
const add = (a, b) => {
  return a + b;
}

//Функция - вычитание
const subtraction = (a, b) => {
	return a - b;
}

//Функция - умножение
const multiple = (a, b) => {
  return a * b;
}

//Функция - деление
const division = (a, b) => {
  if (a == 0 || b == 0) {
    return alert("ошибка, значение не должно быть равно 0");
  } else {
    return a / b;
  }
}

//Функция - калькулятор
function calculate(a, b, callback) {
  if (arguments.length != 3) {
    console.log(`Количество аргументов должно быть равно 3 ! Вы ввели ${arguments.length} `);
  } else {
    result = callback(a, b);
  }
}

//Функция - ввод данных
function input() {
  х1 = parseInt(prompt("Введите первое число", ""));
  sign = prompt("Введите знак операции +, -, *, /", "");
  х2 = parseInt(prompt("Введите второе число", ""));
}

//Функции - проверки данных
function verifyNan() {
  х1 = isNaN(х1) ? 0 : х1;
  х2 = isNaN(х2) ? 0 : х2;
}

const verify = () => {
  switch (sign) {
    case '+':
      calculate(х1, х2, add);
      break;
      
    case "-":
      calculate(х1, х2, subtraction);
      break;
    
    case "*":
      calculate(х1, х2, multiple);
      break;

    case "/":
      calculate(х1, х2, division);
      break;
    default:
      alert(`${sign} - не является арифметическим знаком.`);
  }
}

//Функция - вывод результата
function show() {
  document.getElementById("result").innerHTML = `<p> ${х1} ${sign} ${х2} = ${result} </p>`;
}

//Функция - выполнения заачи
function start() {
  input();
  verifyNan();
  verify();
  calculate();
  result = result === undefined ? alert('Ошибка ввода данных,введите правильные данные') : show();
}

btn.onclick = start;






