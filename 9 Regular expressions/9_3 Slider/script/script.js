
window.onload = ()=>{

  //Переменной box присваиваем div в котором будут появляться слайды.    
  const box = document.getElementById('box_img'),

  //Создаем массив всех адресов слайдов.
        imgHttps = ["https://naked-science.ru/wp-content/uploads/2022/03/1-1.jpg",
                  "https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg",
                  "https://api.eric.s3storage.ru/super-static/prod/62d18645bab4c55256512185-900x.jpeg",
                  "https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg",
                  "https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg"],
        imgArr = [];

  //Создаем все слайды внутри нашего div, записываем их в массив imgArr.
  for (let i=0; i<imgHttps.length; i++){
    let img = document.createElement ('img');
    box.append(img);
    img.setAttribute ('src', imgHttps[i]);
    imgArr.push(img);
  };   
  let nextImg = 0;

  //Создаем таймер со сменой слайдов каждые 3 секунды.
    setInterval(()=>{
        let imgСhange= imgArr[nextImg++ % imgArr.length];
        box.insertAdjacentElement('afterbegin', imgСhange);   
  }, 3000);
};   









