//Объявляем константы в которые присваиваем все необходимые элементы секундмера через их id.
const getId=(id) => document.getElementById(id),
      bord = getId("bord"),
      hh = getId("hh"),
      mm = getId("mm"),
      ss = getId("ss"),
      startBtn = getId("start"),
      stopBtn = getId("stop"),
      resetBtn = getId("reset");

//Объявляем переменные - секунды, минуты, часы.
let sec = 0,
    min = 0, 
    hour = 0;   

//Основная функция tick()
const tick = ()=>{
  sec++;
  if (sec >= 60) { //Задаем числовые параметры, меняющиеся по ходу работы программы.
      min++;
      sec = sec - 60;
  };
  if (min >= 60) {
      hour++;
      min = min - 60;
  };
  if (sec < 10) { //Визуальное оформление.
      if (min < 10) {
          if (hour < 10) {
              ss.innerText =`0${sec}`;
              mm.innerText =`0${min}`;
              hh.innerText =`0${hour}`;             
          } else {
              ss.innerText =`0${sec}`;
              mm.innerText =`0${min}`;
              hh.innerText = hour;
          };
      } else {
          if (hour < 10) {
              ss.innerText =`0${sec}`;
              mm.innerText = min;
              hh.innerText =`0${hour}`;
          } else {
              ss.innerText =`0${sec}`;
              mm.innerText = min;
              hh.innerText = hour;
          };
      };
  } else {
      if (min < 10) {
          if (hour < 10) {
              ss.innerText = sec;
              mm.innerText =`0${min}`;
              hh.innerText =`0${hour}`;
          } else {
              ss.innerText = sec;
              mm.innerText =`0${min}`;
              hh.innerText = hour;
          }
      } else {
          if (hour < 10) {
              ss.innerText = sec;
              mm.innerText = min;
              hh.innerText = `0${hour}`;
          } else {
              ss.innerText = sec;
              mm.innerText = min;
              hh.innerText = hour;
          };
      };
  };
};

//Функция запуска таймера секундомера при нажатии кнопки START.
let timer;
startBtn.onclick = ()=>{
    timer = setInterval (tick, 1000);
    //смена цвета контура секундамера и его кнопок на зеленый.
    bord.classList.remove("black", "red", "silver");
    bord.classList.add("green");
    startBtn.classList.remove("black", "red", "silver");
    startBtn.classList.add("green");
    stopBtn.classList.remove("black", "red", "silver");
    stopBtn.classList.add("green");
    resetBtn.classList.remove("black", "red", "silver");
    resetBtn.classList.add("green");
    //Блокирую нажатие кнопки START повтоно.
    startBtn.disabled = true;
};

//Функция остановки таймера секундомера при нажатии кнопки STOP.
stopBtn.onclick = ()=>{
    clearInterval (timer);
    //смена цвета контура секундамера и его кнопок на красный.
    bord.classList.remove("black", "green", "silver");
    bord.classList.add("red");
    startBtn.classList.remove("black", "green", "silver");
    startBtn.classList.add("red");
    stopBtn.classList.remove("black", "green", "silver");
    stopBtn.classList.add("red");
    resetBtn.classList.remove("black", "green", "silver");
    resetBtn.classList.add("red");
    //Разблокирую нажатие кнопки START.
    startBtn.disabled = false;
};

//Функция сброса таймера секундомера при нажатии кнопки RESET.
resetBtn.onclick = ()=>{
    clearInterval (timer);
    //Обнуление таймера на дисплее секундомера.
    ss.innerText = `00`;
    mm.innerText = `00`;
    hh.innerText = `00`;
    //смена цвета контура секундамера и его кнопок на серый.
    bord.classList.remove("black", "green", "red");
    bord.classList.add("silver");
    startBtn.classList.remove("black", "green", "red");
    startBtn.classList.add("silver");
    stopBtn.classList.remove("black", "green", "red");
    stopBtn.classList.add("silver");
    resetBtn.classList.remove("black", "green", "red");
    resetBtn.classList.add("silver");
    //Разблокирую нажатие кнопки START.
    startBtn.disabled = false;
    //Обнуление переменных - секунды, минуты, часы.
    sec = 0, min = 0, hour = 0; 
};

