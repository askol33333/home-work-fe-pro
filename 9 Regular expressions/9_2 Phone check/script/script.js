//Присваиваем переменным созданые элементы HTML страницы.
const inputTel = document.createElement("input"),
      btn = document.createElement("input"),    
      divError = document.createElement("div");
      div = document.createElement("div");

//Присваиваем нужные атрибуты для созданых элементов HTML страницы. 
inputTel.setAttribute("type", "tel");
inputTel.setAttribute("placeholder", "000-000-00-00");

btn.setAttribute("type", "button");
btn.setAttribute("value", "СОХРАНИТЬ");

divError.innerText = "Ошибка! Не верный формат номера телефона."    

//Выводим на HTML страницы созданые элементы.
document.body.prepend(inputTel);
document.body.append(document.createElement("br"));
document.body.append(btn);

//Проверка номера телефона.
let testTel = /^\d{3}-\d{3}-\d{2}-\d{2}$/;

//Функция перехода на страницу.
const nextPage = () => {
    document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
};

// Функция события при нажатии на поле ввода номера телефона. 
inputTel.onclick = ()=>{
    inputTel.classList.remove('no');
    divError.replaceWith(div);
};

//Функция события при нажатии на кнопку btn.
btn.onclick = ()=>{
    if (testTel.test(inputTel.value)){ 
        inputTel.classList.add("yes");
        setTimeout(nextPage, 2000);
    } else {
        document.body.prepend(divError);
        inputTel.classList.add("no");
    }; 
};


