
// Создаем массив.
let arr1 = [1, 0, 3, -4, 5.878, 6, -7, 8.88, 9, 101],
	
// Создаем переменную ввода числа пользователем.	  
    enteredNumber = 10;
	  
// Создаем метод (функцию) перемножения элементов массива на заданное число.
const multiplyBy = function(arr,eN){
	return arr.map((e)=>e*eN);	
}

// Проверяем работу метода.
console.log(arr1);
console.log(enteredNumber);
let newArr = multiplyBy(arr1,enteredNumber); // вызываем метод умножения на заданное число и результаты записываем в переменную newArr.
console.log(newArr);
console.log("--------------------------");

// Создаем данный метод для каждого массива - через Array и записываем его в Prototype.
Array.prototype.globalMultiplyBy = function(eN){
	return this.map((e)=>e*eN)
}

// Проверяем работу глобального метода globalmultiplyBy с новым тестовым массивом.
const arrTest = [15, 21, 0, -12, 3.8, 6, -157, 7, 10, -56];
console.log(arrTest);
console.log(enteredNumber);
let newArrTest = arrTest.globalMultiplyBy(enteredNumber); // вызываем глобальный метод умножения на заданное число и результаты записываем в переменную newArrTest.
console.log(newArrTest);

