// Объяваляем метод (функция) обращения к тегу по его id, и метод (функции) обращения к тегу по его типу.
const gEBI = id => document.getElementById(id),
      qS = el => document.querySelector(el);

 // Создаем объект с базой цен и названий на все возможные части пиццы.
 let pricePizza = {
    pizzaBaze: {
        small: {price: '100'},
        mid: {price: '200'},
        big: {price: '300'}
    },
    sauces: {
        sauceClassic: {name: 'Кетчуп', price: '10'},
        sauceBBQ: {name: 'BBQ', price: '20'},
        sauceRikotta: {name: 'Рiкотта', price: '30'}
    },       
    toppings: {
        moc1: {name: 'Сир звичайний', price: '40'},
        moc2: {name: 'Сир фета', price: '50'},
        moc3: {name: 'Моцарелла', price: '60'},
        telya: {name: 'Телятина', price: '70'},
        vetch1: {name: 'Помiдори', price: '20'},
        vetch2: {name: 'Гриби', price: '30'}
    }
};

let formPizzaId, allPrice, imgSrc, idSauce, idToppings, isDrawing = false, notSauce = true, notToppings = true, sauce = [], topping = [];

// Запуск функции init при выполнении события загрузка контента сртраницы.  
window.addEventListener("DOMContentLoaded", init);

// Объявляем главную функцию кода (init).
function init() {

    const formPizza = gEBI('pizza'), 
          pizzaPrice = qS('.price > p'),
          banner = gEBI("banner"), 
          inputName = qS('.grid > input:nth-of-type(1)'),
          inputTel = qS('.grid > input:nth-of-type(2)'),
          inputEmail = qS('.grid > input:nth-of-type(3)'),
          btnCancel = qS('.grid > input:nth-of-type(4)'),
          btnSubmit = qS('.grid > input:nth-of-type(5)'),
          ingridients = qS('.ingridients'),  
          table = qS('.table'),
          saucesOut = qS('.sauces > p'),
          toppingsOut = qS('.toppings > p');
          
    allPrice = (+`${pricePizza.pizzaBaze.big.price}`);
    pizzaPrice.innerText = `ціна: ${allPrice} грн`;// устанавливаем цену пиццы по умолчанию (при загрузке страницы).

    // Слушаем событие - клик по индикаторам размера основы пиццы и при выборе устанавливаем цену основы пиццы. 
    formPizza.addEventListener("click", function (e) {
        cancelIngridients ();
        formPizzaId = e.target.id;
        allPrice = (+`${pricePizza.pizzaBaze[formPizzaId].price}`); //Что тут не так не пойму? Вроде все работает.
        pizzaPrice.innerText = `ціна: ${allPrice} грн`; 
    });
   
    // Слушаем событие - наведение курсора на скидку, по итогу скидка убегает от курсора. 
    banner.addEventListener("mouseover", () => {
        banner.style.bottom = `${Math.floor(Math.random()*80)}%`;
        banner.style.right = `${Math.floor(Math.random()*80)}%`;
    });
        
    // Проверка введеных данных заказчика пиццы.
    document.querySelectorAll(".grid > input").forEach((e) => {
        e.addEventListener("change", (e) => { 
            if (validate(e)){
                e.target.classList.remove('error');
                e.target.classList.add('success');
            } else{
                e.target.classList.remove('success');
                e.target.classList.add('error');                
            };
        });
    });

     

    // Слушем нажатие кнопки регистрации, и выполняем проверку.
    btnSubmit.addEventListener("click", (e) => {
        let invalid = false;
        document.querySelectorAll(".grid > input").forEach((el) => {
            if(el.type !== 'button' || el.type !== 'reset'){
                if (el.classList.contains("error") || el.value === '' || notSauce || notToppings){        
                invalid = true;
                }
                console.log (`Тип:${el.type} Дані:${el.value} ${'<br>'}`);
            }; 
        });
        if (invalid){
            return false;
        }   
        window.location.href = './thank-you.html'; //переход на страницу "thank-you".; 
        // ***** место для вызова функции отправки данных по заказу на сервер для обработки заказа******.  
    });

    // Фукция проверки данных введенных заказчиком.
    function validate(event) {
        switch (event.target.name) {
            case "name": return /^[А-яІіЄєЇїҐґ']{3,}$/.test(event.target.value);
            case "phone": return /^\+380\d{9}$/.test(event.target.value);
            case "email": return /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(event.target.value);
        };
    };

    // Функция сброса выбраных соусов и топингов.
    function cancelIngridients (){
        notSauce = true;
        notToppings = true;
        saucesOut.innerText = `cоуси:`; 
        toppingsOut.innerText = `Топiнги:`;
        sauce = [];
        topping = [];
        document.querySelectorAll(".table > img").forEach((e) => {
            if(e.classList.contains('img-pizza')) {
                e.remove();  
            };  
        });
    };
    
    // Функция события при нажатии на кнопку btnCancel. Сброс заказа.
    btnCancel.addEventListener("click", ()=>{
        inputName.classList.remove('error');
        inputTel.classList.remove('error');
        inputEmail.classList.remove('error');
        cancelIngridients ();
        allPrice = (+`${pricePizza.pizzaBaze[formPizzaId].price}`);
        pizzaPrice.innerText = `ціна: ${allPrice} грн`;  
    });

    // Слушем нажатие кнопки для выбора ингредиента пиццы.
    ingridients.addEventListener('mousedown', (e) => {
        if( e.target.classList.contains('draggable') ) {			
            imgSrc = e.target.src;
            console.log('img.src: ' + imgSrc);			
            if( e.target.id.includes('sauce') )	{				
                idSauce = e.target.id;
                console.log('sauce:' + idSauce);
            } else {
                idToppings = e.target.id;
                console.log('toppings:' + idToppings);
            };
        };
        isDrawing = true;
    });

    // Слушаем событие нажатия кнопки на месте вставки ингредиента в пиццу.
    table.addEventListener('mouseup', () => {
        
        if(isDrawing) {
            const img = document.createElement('img');
            img.classList.add('img-pizza');
            img.src = `${imgSrc}`;
            table.append(img); 
            if(idSauce in pricePizza.sauces){
                notSauce=false;
                sauce.push(`${pricePizza.sauces[idSauce].name}`);
                saucesOut.innerText = `cоуси:\r\n ${ sauce.join('\r\n') }`;
                allPrice += (+`${pricePizza.sauces[idSauce].price}`);
                pizzaPrice.innerText = `ціна: ${allPrice} грн`;
                idSauce = '';			
            } else {
                notToppings=false;
                topping.push(`${pricePizza.toppings[idToppings].name}`);
                toppingsOut.innerText = `Топiнги:\r\n ${topping.join('\r\n')}`;
                allPrice += (+`${pricePizza.toppings[idToppings].price}`);
                pizzaPrice.innerText = `ціна: ${allPrice} грн`;
                idToppings = '';			
            };
            isDrawing = false;
        };
    });
};
