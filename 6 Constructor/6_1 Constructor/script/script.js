 /*
Разработайте функцию-конструктор которая будет создавать объект Human(человек). 
Создайте массив объектов и реализуте функцию, которая будет сортировать элементы 
массива по значению свойства Age, по возрастанию или по убыванию.
*/

// Создаем функцию-конструктор - Human.
function Human (name, surname, age, gender) {
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.gender = gender;
 
 //Создаем метод в функции-конструкторе для вывода данных всех экземпляров.
    this.print = function () {
        if (Array.isArray(arrHuman)) {

            //Вывод на экран свойств объекта с помощью переборки массива.
            for (let element of arrHuman) {
                document.write(`<p> Name: ${element.name} <br> Surname: ${element.surname} <br> Age: ${element.age} <br> Пол: ${element.gender} <br></p>`);
            }
        };
    };
};
 
//Создаем массив объектов (экземпляров функции-конструктора Human).
const arrHuman=[]; 

// Вводим количество человек которое готовы добавить в базу.
let a = prompt("Введите колличество человек которые будут добавлены в базу.","0");

// Вводим данные людей, в количестве заданном счетчиком. 
// Данные каждого человека во вложенных массивах, база всех человек в главном массиве.
// Масив базы данных людей является экземпляром функции-конструктора Human.
for (let i=0; i<a; i++){
    let name = prompt("Введите имя "+ (i+1) +"-го человека.","Ivan/Olga");
    let surname = prompt("Введите фамилию "+ (i+1) +"-го человека.","Ivanov/Sidorova");
    let age = prompt("Введите возраст "+ (i+1) +"-го человека.","00");
    let gender = prompt("Введите пол "+ (i+1) +"-го человека.","m/w");
        
    arrHuman[i] = new Human(name, surname, age, gender);
};

//Создаем функцию для сортировки людей по значению элемента Age по возрастанию.
function sortArrUpAge(a, b) {
    return a.age > b.age ? 1 : b.age > a.age ? -1 : 0;
};

//Создаем функцию для сортировки людей по значению элемента Age по убыванию.
function sortArrDownAge(a, b) {
    return a.age < b.age ? 1 : b.age < a.age ? -1 : 0;
};

//Выводим данные работы кода на экран + вызов функций по сортировке.
document.write(`<h2> Выводим данные базы людей. </h2>`);
arrHuman[0].print();
arrHuman.sort(sortArrUpAge);
document.write(`<h2> Сортировка по возростанию. </h2>`);
arrHuman[0].print();
arrHuman.sort(sortArrDownAge);
document.write(`<h2> Сортировка по убыванию. </h2>`);
arrHuman[0].print();

