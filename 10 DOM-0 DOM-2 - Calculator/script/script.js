//Запускаем главную функцию кода (init) после загрузки контента страницы.
window.addEventListener("DOMContentLoaded", init);

//Объявляем главную функцию кода (init).
function init() {

    // Объявляем константы.
    //btn - присваиваем область всех кнопок калькулятора.
    //display - присваиваем область экрана калькулятора.
    const btn = document.querySelector(".keys"),
          display = document.querySelector(".display > input"),
          result = document.querySelector(".orange");
    
    //Объявляем все необходимые глобальные переменные.
    let a="",
        b="", 
        action="", 
        flag = false,
        res="",
        mPlus="",
        counter = 0;
    
    display.value = 0;

    //Создаём букву 'm' для вывода ее на дисплей при сохранении числа в ячейке памяти.
    const mBox = document.createElement('span');
    mBox.setAttribute('id', 'm');
    document.querySelector('.display').prepend(mBox);

    //Двойной клик на кнопку mrc. Оставил на всякий случай.
    /*btn.addEventListener("dblclick", function (el) {
        if (el.target.classList.contains('gray')) {
            if (el.target.value === "mrc"){
                mPlus =0;
                display.value = 0;
                mBox.innerText = "";
            }
        }  
    })*/  

    /*******************************///Выполняем функцию при появлении события click на кнопках калькулятора.
    btn.addEventListener("click", function (e) {
        
        //Условная конострукция нажатия любой из кнопок калькулятора.
        //Работа кнопки сброс "С".
        if ( e.target.value === "C") {
            a = '';
            b = '';
            action = '';
            display.value = 0;
            flag = false;
            result.setAttribute('disabled', '');
            res = "";
/**/        console.log("flag a/b= " + flag,"a= " + a, "b= " + b,"action= " + action ,"res= " + res, "mPlus= " + mPlus);

        //Вводим числа и показываем их на экране, и присваиваем их в переменные "a" и "b".
        } else if ( e.target.classList.contains('black') ) {

            let number = e.target.value;

    		if(b === "" && action === ""){		
				a += number;		
				display.value = a;
			} else if(a!=="" && b==="" && action!==""){
				b += number;
				display.value = b;
                //Условие разблокировки кнопки "=".
                unlockEquals();
			} 
/**/        console.log("flag a/b= " + flag,"a= " + a, "b= " + b,"action= " + action ,"res= " + res, "mPlus= " + mPlus);

        //Вводим действие и не показываем его на экране, и присваиваем его в переменную action.
        } else if (e.target.classList.contains('pink')) {
            action = e.target.value;
            flag = true;
            if (a !== "" && b !== ""){
                resultOutput();
            }      
/**/        console.log("flag a/b= " + flag,"a= " + a, "b= " + b,"action= " + action ,"res= " + res, "mPlus= " + mPlus);
            
        //Обрабатываем кнoпки с ячайками памяти.   
        } else if (e.target.classList.contains('gray')) {
            if (e.target.value === "m+"){
                mPlus = display.value;
                mBox.innerText = "m";
            } else if (e.target.value === "m-") {
                mPlus = mPlus - display.value;
                mBox.innerText = "m";
            } else if (e.target.value === "mrc"){
                    counter++;
                if (counter == 1) {
                    display.value = mPlus;
                    if (flag === true){
                        b = mPlus;
                        unlockEquals();
                        flag = false;  
                    } else if (flag === false){
                        a = mPlus;
                        action="";
                        result.setAttribute('disabled', '');
                        flag = true;
                    }
                } else if (counter == 2) {
                    mPlus = 0;
                    display.value = 0;
                    mBox.innerText = "";
                    counter = 0;
                }
            }
/**/        console.log("counter= " + counter,"flag a/b= " + flag,"a= " + a, "b= " + b,"action= " + action ,"res= " + res, "mPlus= " + mPlus);
                
        //Нажатие кнопки "=".    
        } else if (e.target.classList.contains('orange')) {
            resultOutput();  
        }     
    })

    //Функция вывода и сохранения результата.
    const resultOutput = () =>{
        performingAnAction();
        display.value = res; 
        result.setAttribute("disabled","");
        a=res;
        flag=true;
        b="";
/**/    console.log("flag a/b= " + flag,"a= " + a, "b= " + b,"action= " + action ,"res= " + res, "mPlus= " + mPlus);
    }

    //Функция разблокировки кнопки "=".
    const unlockEquals = () =>{
        if (b !== "") {
            result.removeAttribute("disabled"); 
        }	
    }
    
    //Набор функций для вычисления результата.
    //Функция - сложение.
    const add = (a, b) => {
        return (+a) + (+b);
    }
      
    //Функция - вычитание.
    const subtraction = (a, b) => {
        return a - b;
    }
      
    //Функция - умножение.
    const multiple = (a, b) => {
        return a * b;
    }
      
    //Функция - деление.
    const division = (a, b) => {
        if (b == 0) {
            result.setAttribute('disabled', '');
			return 'error';
        } else {
            return a / b;
        }
    }
     
    //Функция - калькулятор.
    function calculate(a, b, operation) {
        res = operation(a, b);
    }

    //Функция выполнения введенного действия.
    const performingAnAction = () => {
        switch (action) {
            case '+':
                calculate(a, b, add);
                break;
            case "-":
                calculate(a, b, subtraction);
                break;
            case "*":
                calculate(a, b, multiple);
                break;
            case "/":
                calculate(a, b, division);
                break;
        }
    }
}   