// Домашгяя работа
document.write("<h3>Домашняя работа</h3>" + "<p>");

let styles = ["Джаз","Блюз"];
document.write("Создали массив styles: [ " + styles.join (" - ") + " ], длинной: " + styles.length + " элемента." + "<p>");

styles.push("Рок-н-ролл");
document.write("Добавили в конец массива еще один элемент: " +  styles[styles.length-1] + "." + "<br>");
document.write("Получаем обновленный массив с элементами: [ " + styles.join (" - ") + " ], длинной: " + styles.length + " элемента." + "<p>");

let middle = styles.length / 2;
styles.splice(Math.floor(middle), 1, "Классика");
document.write("Заменили средний элемент нашего массива имеющий индекс : " + Math.floor(middle) + ", на стиль: " + styles[Math.floor(middle)] + "." + "<br>");
document.write("Получаем обновленный массив с элементами: [ " + styles.join (" - ") + " ], длинной: " + styles.length + " элемента." + "<p>");

document.write("Удалили первый элемент массива: " + styles.splice(0,1) + "." + "<br>")
document.write("Получаем обновленный массив с элементами: [ " + styles.join (" - ") + " ], длинной: " + styles.length + " элемента." + "<p>");


styles.unshift("Рэп", "Регги");
document.write("Добавили в начало массива два элемента: " + styles[0] + ", " + styles[1] + "." + "<br>")
document.write("Получаем обновленный массив с элементами: [ " + styles.join (" - ") + " ], длинной: " + styles.length + " элемента." + "<p>");

// Классная работа
document.write("<h3>Классная работа</h3>" + "<p>");

let i, j =15, arr = new Array(j); 
for (i=0; i<j; i++){
    arr[i]=i+1
}
document.write("Получаем массив с элементами: [ " + arr.join (" - ") + " ], длинной: " + arr.length + " элементов." + "<p>");
arr.reverse();
document.write("Выводим массив в обратном порядке: [ " + arr.join (" &#9729 ") + " ], длинной: " + arr.length + " элементов." + "<p>");

let index = prompt("Введите номер индека", "");
arr.splice(index, 1);
document.write("Выводим массив без удаленного элемента: [ " + arr.join (" &#9729 ") + " ], длинной: " + arr.length + " элементов." + "<p>");
        