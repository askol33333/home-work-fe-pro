/*
Перевірка даних.

        1. Використовуючи JS, створіть 5 полів для води даних і залиште їх. 
        2. Поля:
        Ім'я (Укр. Літери), Номер телефону у форматі +38ХХХ-ХХХ-ХХ-ХХ, електронна пошта, пароль та підтвердження пароля (паролі повинні співпадати).
        
        3. Додайте стилі на помилку і стиль на правильне введення.
        4. Реалізуй перевірку даних.
        При введенні даних відразу перевіряти на правильність і виводити помилку, якщо така необхідна.
        додай кнопку реєстрація, при натисканні кнопки перевірити всі поля та вивести помилку якщо така буде.
        
        Збереження.
        5. Якщо користувач все записав правильно, то збережіть дані на клієнті із зазначенням дати та часу збереження.
        
        Для стилізації використовуй CSS класи. Для створення елементів використовуй JS.
*/
/*
function addEventListener (event, Listener) {
    event > Listener
}
*/


window.addEventListener("DOMContentLoaded", () => {
    document.body.prepend(inputs.name, inputs.phone, inputs.email, inputs.password, inputs.passwordRepeat, inputs.btn);
    
    const btn1 = document.querySelector('input[name=btn]');
    
    document.querySelectorAll("input")
        .forEach((e) => {
            const span = document.createElement("span");
            e.after(span);
            e.after(document.createElement("br"));
            span.setAttribute("id", e.name);

            e.addEventListener("change", (e) => { 
                if (validate(e)){
                    e.target.classList.remove('error');
                    e.target.classList.add('valid');
                    span.innerText = "";
                } else{
                    e.target.classList.remove('valid');
                    e.target.classList.add('error');                
                    span.innerText = "УВАГА! Помилка під час введення даних!"
                };
            });
        });

    // слухаємо натиск кнопки реєстрація, та виконуємо перевірку.
    btn1.addEventListener("click", () => {
        document.querySelectorAll("input")
            .forEach((el) => {
                if(el.type !== 'button'){
                    if (el.classList.contains("error") || el.value === ""){
                        document.getElementById('btn').innerText = "УВАГА! Реєстрація не пройшла! Помилка під час введення даних!";
                        btn1.classList.remove('valid');
                        btn1.classList.add('error');
                    } else if(el.classList.contains("valid")){
                        document.getElementById('btn').innerText = "";
                        btn1.classList.remove('error');
                        btn1.classList.add('valid');
                        saveInfo(el.type, el.value); // запуск функції збереження даних. 
                    };   
                }    
        });
    });
});

// create html input
function cretaeInput(type = "number", className = "", placeholder = "", name = "") {
    const input = document.createElement("input"),
        div = document.createElement("div"),
        label = document.createElement("label"),
        p = document.createElement("p"),
        id = `input${Math.random() * 1000000}`;

    input.id = id;
    label.setAttribute("for", id);
    input.type = type;
    input.name = name;
    input.className = className;
    label.innerText = placeholder;
    div.prepend(label);
    label.after(input);
    div.append(p);

    if (type === "button") {
        input.value = placeholder;
        label.innerText = '';
    } else {
        input.placeholder = placeholder;
    }
    return div;
};

// перевірка даних
function validate(event) {
    switch (event.target.name) {
        case "text": return /^[А-яіґєїІҐЄЇ']+$/.test(event.target.value);
        case "tel": return /^\+380\d{2}-\d{3}-\d{2}-\d{2}$/.test(event.target.value);
        case "email": return /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(event.target.value);
        case "password": return /^[A-z0-9]+$/.test(event.target.value); 
        case "passwordRepeat": return document.querySelectorAll(`input[type="password"]`)[0].value === document.querySelectorAll(`input[type="password"]`)[1].value;
    };
};

// стврення інпутів. 
const inputs = {
    name: cretaeInput("text", "user-info", "input your name", "text"),
    phone: cretaeInput("tel", "user-info", "input your phone","tel"),
    email: cretaeInput("email", "user-info", "input your email","email"),
    password: cretaeInput("password", "user-info", "input your password","password"),
    passwordRepeat: cretaeInput("password", "user-info", "Repeat password","passwordRepeat"),
    btn: cretaeInput("button", "btn", "Реєстрація","btn")
},

// збереження даних.
saveInfo = (keys, information)=>{
    window.sessionStorage.setItem(keys, information);

    const date1 = new Date(),
        recordingDate = `${date1.getDate()}.${(date1.getMonth())+1}.${date1.getFullYear()}`,
        recordingTime = `${date1.getHours()}:${date1.getMinutes()}:${date1.getSeconds()}`;
                  
    window.sessionStorage.setItem("recording date", recordingDate);
    window.sessionStorage.setItem("recording time", recordingTime);
};
